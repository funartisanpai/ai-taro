export default definePageConfig({
  navigationBarTitleText: 'AI助手',
  navigationBarBackgroundColor: '#2196f3',
  backgroundColor: '#2196f3',
  backgroundTextStyle: 'dark'
})
