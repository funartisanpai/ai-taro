import { Component, PropsWithChildren } from 'react'
import {View, Text, Input, Icon} from '@tarojs/components'
import './index.scss'

export default class Index extends Component<PropsWithChildren> {

  componentWillMount () { }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

  render () {
    return (
      <View className='chat'>
        <View className="chat-list">
          <View className="chat-item">
            <View className="time">2022-10-12 12:00:59</View>
            <View className="user">
              <Text>sdfsdf</Text>
            </View>
            <View className="roboot">
              <Text>sdfsdf</Text>
            </View>
          </View>
          <View className="chat-item">
            <View className="time">2022-10-12 12:00:59</View>
            <View className="user">
              <Text>sdfsdf</Text>
            </View>
            <View className="roboot">
              <Text>sdfsdf</Text>
            </View>
          </View>
          <View className="chat-item">
            <View className="time">2022-10-12 12:00:59</View>
            <View className="user">
              <Text>sdfsdf</Text>
            </View>
            <View className="roboot">
              <Text>sdfsdf</Text>
            </View>
          </View>
          <View className="chat-item">
            <View className="time">2022-10-12 12:00:59</View>
            <View className="user">
              <Text>sdfsdf</Text>
            </View>
            <View className="roboot">
              <Text>sdfsdf</Text>
            </View>
          </View>
          <View className="chat-item">
            <View className="time">2022-10-12 12:00:59</View>
            <View className="user">
              <Text>sdfsdf</Text>
            </View>
            <View className="roboot">
              <Text>sdfsdf</Text>
            </View>
          </View>
          <View className="chat-item">
            <View className="time">2022-10-12 12:00:59</View>
            <View className="user">
              <Text>sdfsdf</Text>
            </View>
            <View className="roboot">
              <Text>sdfsdf</Text>
            </View>
          </View>
          <View className="chat-item">
            <View className="time">2022-10-12 12:00:59</View>
            <View className="user">
              <Text>sdfsdf</Text>
            </View>
            <View className="roboot">
              <Text>sdfsdf</Text>
            </View>
          </View>
        </View>
        <View className='chat-input'>
          <Input placeholder='请输入问题'></Input>
          <Icon size='20' type='search' />
        </View>
      </View>
    )
  }
}
