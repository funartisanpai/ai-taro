export default defineAppConfig({
  pages: [
    'pages/index/index'
  ],
  window: {
    backgroundTextStyle: 'light',
    navigationBarBackgroundColor: '#fff',
    navigationBarTitleText: 'WeChat',
    navigationBarTextStyle: 'black'
  },
  subPackages: [
    {
      root: "chat",
      name: "chat",
      independent: true,
      pages: [
        'index/index'
      ]
    },
    {
      root: "user",
      name: "user",
      independent: true,
      pages: [
        'index/index'
      ]
    }
  ]
})
