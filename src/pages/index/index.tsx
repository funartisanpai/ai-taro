import { Component, PropsWithChildren } from 'react'
import { View, Text } from '@tarojs/components'
import './index.scss'
import Taro from "@tarojs/taro";

const clickGo = function (params: number): boolean {
  let urlObject: string[] = [
    '/chat/index/index',
    '/user/index/index',
  ]
  Taro.navigateTo({
    url: urlObject[params]
  })
  return true
}

export default class Index extends Component<PropsWithChildren> {

  componentWillMount () { }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

  render () {
    return (
      <View className='index'>
        <View className='desc'>
          <View className='desc-title'>AI 助手</View>
          <View className='desc-content'>宇宙是一个答案，我们应该找到的是问题，关 于宇宙这个答案的问题，问题才是困难的部 分，如果能够提出合适的问题，那答案相对来 说就简单了，终有一天我们会死去，我希望我 们是走在理解宇宙，理解生命意义的道路上 。</View>
        </View>
        <View className='operate'>
          <Text
            className='operate-btn'
            onClick={()=>clickGo(0)}
          >
            AI对话
          </Text>
          <Text
            className='operate-btn'
            onClick={()=>clickGo(1)}>
            个人中心
          </Text>
        </View>
      </View>
    )
  }
}
