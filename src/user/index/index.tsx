import { Component, PropsWithChildren } from 'react'
import { View, Text, Input, Form } from '@tarojs/components'
import './index.scss'

const formSubmit = function () {
    console.log('111')
}
const gotoCount = function () {
    console.log('ququ')
}
export default class Index extends Component<PropsWithChildren> {

  componentWillMount () { }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

  render () {
    return (
      <View className='user'>
        <View className='message'>基本信息</View>
        <View className='form'>
          <Form onSubmit={formSubmit}>
             <View className='phone'>
               <View className='label'>手机号</View>
               <Input placeholder='请输入'></Input>
             </View>
             <View className='phone'>
               <View className='label'>密码</View>
               <Input placeholder='请输入'></Input>
             </View>
             <View className='phone'>
               <View className='label'>确认密码</View>
               <Input placeholder='请输入'></Input>
             </View>
             <View className='phone'>
               <View className='label'>昵称</View>
               <Input placeholder='请输入'></Input>
             </View>
             <View className='phone'>
               <View className='label'>邮箱</View>
               <Input placeholder='请输入'></Input>
             </View>
             <View className='save' onClick={formSubmit}>保存</View>
          </Form>
        </View>
        <View className='key'>
            <View className='left'>
                <View className='label'>API KEY状态</View>
                <View className='label'>账户类型</View>
                <View className='label'>到期时间</View>
                <View className='label'>剩余次数/总次数</View>
                <View className='label'>最后登录</View>
            </View>
            <View className='mid'>
                <View className='disc'>未启用</View>
                <View className='disc'>未启用未启用</View>
                <View className='disc'>未启用</View>
                <View className='disc'>未启用</View>
                <View className='disc'>未启用未启用</View>
            </View>
            <View className='right'>
                <View onClick={gotoCount} className='count'>升级账户</View>
            </View>
        </View>
      </View>
    )
  }
}
